//
//  GoodreadsSearchResponse.swift
//  book-catalogue
//
//  Created by Zachary Smith on 11/9/19.
//  Copyright © 2019 Zachary Smith. All rights reserved.
//

import Foundation

struct GoodreadsSearchResponse: Codable {
    var search: GoodreadsSearch? = nil
}

struct GoodreadsSearch: Codable {
    var results: [GoodreadsWork]? = nil
}

struct GoodreadsWork: Codable {
    var id: Int? = nil
    var booksCount: Int? = nil
    var ratingsCount: Int? = nil
    var textReviewsCount: Int? = nil
    var originalPublicationYear: Int? = nil
    var originalPublicationMonth: Int? = nil
    var originalPublicationDay: Int? = nil
    var averageRating: Double? = nil
    var book: GoodreadsBook? = nil
}


struct GoodreadsBook: Codable {
    var title: String? =  nil
    var imageUrl: String? =  nil
    var author: GoodreadsAuthor? =  nil
    var smallImageUrl: String? = nil
}

struct GoodreadsAuthor: Codable {
    var name: String? = nil
}
