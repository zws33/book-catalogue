//
//  Book.swift
//  book-catalogue
//
//  Created by Zachary Smith on 11/19/19.
//  Copyright © 2019 Zachary Smith. All rights reserved.
//

import Foundation

class Book {
    let title: String
    let author: String
    
    init(title: String, author: String) {
        self.title = title
        self.author = author
    }
}
