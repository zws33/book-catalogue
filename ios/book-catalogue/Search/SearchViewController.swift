//
//  SearchViewController.swift
//  book-catalogue
//
//  Created by Zachary Smith on 11/11/19.
//  Copyright © 2019 Zachary Smith. All rights reserved.
//

import UIKit
import Shared

class SearchViewController: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchResultsTableView: UITableView!
    
    var searchResults: [Book] = []
    
    let goodreadsService: GoodreadsService = getGoodreadsService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchResultsTableView.delegate = self
        searchResultsTableView.dataSource = self
        searchBar.delegate = self
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        guard let searchText = searchBar.text, !searchText.isEmpty else {
            return
        }
        
        
        let handleSuccess: ([BookCatalogueBook]?) -> Void = { bookCatalogueBook in
            let books =  bookCatalogueBook?
                .compactMap { $0 }
                .map { (book: BookCatalogueBook) -> Book in
                    Book(
                        title: book.title ?? "",
                        author: book.author?.name ?? ""
                    )
            }
            self.searchResults = books ?? []
            self.searchResultsTableView.reloadData()
        }
        
        let handleFailure: (String?) -> Void = { message in
            
        }
        
        goodreadsService.search(for: searchText, onSuccess: handleSuccess, onFailure: handleFailure)
    }
}

extension SearchViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let book = searchResults[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResult") as! SearchResultTableViewCell
        
        cell.title.text = book.title
        cell.author.text = book.author
        
        return cell
    }
}
