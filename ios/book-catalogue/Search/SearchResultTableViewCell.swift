//
//  BookTableViewCell.swift
//  book-catalogue
//
//  Created by Zachary Smith on 11/17/19.
//  Copyright © 2019 Zachary Smith. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var author: UILabel!

}
