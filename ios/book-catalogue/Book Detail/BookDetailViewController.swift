//
//  ViewController.swift
//  book-catalogue
//
//  Created by Zachary Smith on 11/9/19.
//  Copyright © 2019 Zachary Smith. All rights reserved.
//

import UIKit
import Shared

class BookDetailViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate {
    
    //MARK: Properties
    @IBOutlet weak var bookTitleLabel: UILabel!
    @IBOutlet weak var bookTitleTextField: UITextField!
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: Actions
    @IBAction func submitBookTitleSearch(_ sender: UIButton) {
        
        let service = getGoodreadsService()
        
        let handleSuccess: ([BookCatalogueBook]?) -> Void = { response in
            self.bookTitleLabel.text = response?.first?.title
            
        }
        let handleFailure: (String?) -> Void = { error in
            guard let error = error else {
                return
            }
            print(error)
        }
        
        if let searchQuery = bookTitleTextField.text, !searchQuery.isEmpty {
            service.search(
                for: searchQuery,
                onSuccess: handleSuccess,
                onFailure: handleFailure)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bookTitleTextField.delegate = self
        
    }
}

