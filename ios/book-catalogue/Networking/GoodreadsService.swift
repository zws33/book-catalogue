//
//  GoodreadsApi.swift
//  book-catalogue
//
//  Created by Zachary Smith on 11/9/19.
//  Copyright © 2019 Zachary Smith. All rights reserved.
//

import Foundation
import Shared

struct GoodreadsApiData {
    static let scheme = "http"
    static let host = "192.168.50.97"
    static let port = 8080
    static let path = "/search"
    static let queryName = "q"
}

protocol GoodreadsService {
    func search(
        for searchText: String,
        onSuccess handleSuccess: @escaping ([BookCatalogueBook]?) -> Void,
        onFailure handleFailure: @escaping (String?) -> Void
    )
}

func getGoodreadsService() -> GoodreadsService {
    return GoodreadsServiceImpl()
}

class GoodreadsServiceImpl : GoodreadsService {
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    var errorMessage: String = ""
    
    private func buildGoodreadsSearchUrl(_ queryValue: String) -> URLComponents {
        var goodreadsUrl = URLComponents()
        goodreadsUrl.scheme = GoodreadsApiData.scheme
        goodreadsUrl.host = GoodreadsApiData.host
        goodreadsUrl.port = GoodreadsApiData.port
        goodreadsUrl.path = GoodreadsApiData.path
        goodreadsUrl.queryItems = [URLQueryItem(name: GoodreadsApiData.queryName, value: queryValue)]
        return goodreadsUrl
    }
    
    func search(
        for searchText: String,
        onSuccess handleSuccess: @escaping ([BookCatalogueBook]?) -> Void,
        onFailure handleFailure: @escaping (String?) -> Void
    ) {
        dataTask?.cancel()
        
        guard let url = buildGoodreadsSearchUrl(searchText).url else {
            handleFailure(errorMessage)
            return
        }
        
        print(url)
        
        let task = defaultSession.dataTask(with: url) { (data, response, error) in
            defer {
                self.dataTask = nil
            }
            if let response = response as? HTTPURLResponse {
                print(response.statusCode)
            }
            var searchResults: [BookCatalogueBook]?
            
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                    searchResults = try decoder.decode([GoodreadsBook].self, from: data).map{ $0.toBookCatalogueBook() }
                    
                    print("searchResults were \(searchResults?.description ?? "nil")")
                } catch let parseError as NSError {
                    self.errorMessage += "JSONSerialization error: \(parseError.localizedDescription)\n"
                    return
                }
            }
            DispatchQueue.main.async {
                handleSuccess(searchResults)
            }
        }
        task.resume()
    }
}

extension Data {
    var prettyPrintedJSONString: String? {
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
            let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
            let prettyPrintedString = String(data: data, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) else { return nil }
        
        return prettyPrintedString
    }
}

extension GoodreadsBook {
    func toBookCatalogueBook() -> BookCatalogueBook {
        return BookCatalogueBook(
            title: self.title,
            imageUrl: self.imageUrl,
            author: self.author?.toBookCatalogueAuthor(),
            smallImageUrl: self.smallImageUrl
        )
    }
}

extension GoodreadsAuthor {
    func toBookCatalogueAuthor() -> BookCatalogueAuthor {
        return BookCatalogueAuthor(name: self.name)
    }
}



