package me.zwsmith.bookcatalogue.models

data class Book(
    val id: String,
    val title: String,
    val imageUrl: String,
    val authors: List<String>,
    val smallImageUrl: String
)

