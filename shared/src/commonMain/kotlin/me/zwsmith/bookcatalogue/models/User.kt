package me.zwsmith.bookcatalogue.models

data class User(
    val id: Int,
    val name: String,
    val password: String
)