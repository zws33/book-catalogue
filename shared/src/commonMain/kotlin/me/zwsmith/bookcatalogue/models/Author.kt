package me.zwsmith.bookcatalogue.models

data class Author constructor(
    val id: Int,
    val name: String
)