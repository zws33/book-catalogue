package me.zwsmith.bookcatalogue

import io.ktor.application.Application

fun Application.getEnvironmentVariable(key: String) = environment.config.property(key).getString()