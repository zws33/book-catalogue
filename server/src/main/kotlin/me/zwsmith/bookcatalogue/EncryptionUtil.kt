package me.zwsmith.bookcatalogue

import org.mindrot.jbcrypt.BCrypt

interface EncryptionUtil {
    fun genSalt(): String
    fun hashPassword(password: String): String?
    fun checkPassword(candidate: String, hashed: String): Boolean
}

@Suppress("FunctionName")
fun EncryptionUtil(): EncryptionUtilImpl {
    return EncryptionUtilImpl
}

object EncryptionUtilImpl : EncryptionUtil {
    override fun genSalt(): String = BCrypt.gensalt()

    override fun hashPassword(password: String): String? {
        return BCrypt.hashpw(password, genSalt())
    }

    override fun checkPassword(candidate: String, hashed: String): Boolean = BCrypt.checkpw(candidate, hashed)
}