package me.zwsmith.bookcatalogue.remote.goodreads

import me.zwsmith.bookcatalogue.models.Author
import me.zwsmith.bookcatalogue.models.Book
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(strict = false, name = "GoodreadsResponse")
data class GoodreadsSearchResponse constructor(
    @field:Element
    var search: Search? = null
) {
    @Root(strict = false, name = "search")
    data class Search constructor(
        @field:ElementList(name = "results", entry = "work")
        var results: MutableList<Work>? = null
    )

    @Root(name = "results")
    data class Work constructor(
        @field:Element
        var id: Int? = null,

        @field:Element(name = "books_count")
        var booksCount: Int? = null,

        @field:Element(name = "ratings_count")
        var ratingsCount: Int? = null,

        @field:Element(name = "text_reviews_count")
        var textReviewsCount: Int? = null,

        @field:Element(name = "original_publication_year", required = false)
        var originalPublicationYear: Int? = null,

        @field:Element(name = "original_publication_month", required = false)
        var originalPublicationMonth: Int? = null,

        @field:Element(name = "original_publication_day", required = false)
        var originalPublicationDay: Int? = null,

        @field:Element(name = "average_rating")
        var averageRating: Double? = null,

        @field:Element(name = "best_book")
        var book: Book? = null
    )


    @Root(strict = false, name = "work")
    data class Book constructor(
        @field:Element
        var id: Int? = null,
        @field:Element
        var title: String? = null,
        @field:Element(name = "image_url")
        var imageUrl: String? = null,
        @field:Element
        var author: Author? = null,
        @field:Element(name = "small_image_url")
        var smallImageUrl: String? = null
    )

    @Root(strict = false, name = "best_book")
    data class Author constructor(
        @field:Element
        var id: Int? = null,
        @field:Element
        var name: String? = null
    )
}

fun GoodreadsSearchResponse.Book.toBook(): Book {
    val id = requireNotNull(id)
    val title = requireNotNull(title)
    val imageUrl = requireNotNull(imageUrl)
    val authorId = requireNotNull(author).id!!
    val smallImageUrl = requireNotNull(smallImageUrl)
    return Book(id.toString(), title, imageUrl, listOf(authorId.toString()), smallImageUrl)
}

fun GoodreadsSearchResponse.Author.toAuthor(): Author {
    val id = requireNotNull(id)
    val name = requireNotNull(name)
    return Author(id, name)
}