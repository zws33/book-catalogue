package me.zwsmith.bookcatalogue.remote.goodreads

import me.zwsmith.bookcatalogue.models.Author
import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(strict = false, name = "GoodreadsResponse")
data class GoodreadsAuthorResponse constructor(
    @field:Element
    var author: Author? = null
) {
    @Root(strict = false, name="author")
    data class Author constructor(
        @field:Attribute(name = "id")
        var id: Int? = null,
        @field:Element
        var name: String? = null
    )
}

fun GoodreadsAuthorResponse.Author.toAuthor(): Author {
    val id = requireNotNull(id)
    val name = requireNotNull(name)
    return Author(id, name)
}