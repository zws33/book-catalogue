package me.zwsmith.bookcatalogue.remote.google

import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.http.Url


@Suppress("FunctionName")
fun GoogleBooksService(client: HttpClient): GoogleBooksService {
    return GoogleBooksServiceImpl(client)
}

interface GoogleBooksService {
    suspend fun search(query: String): GoogleBooksResponse
    suspend fun searchBooks(query: String): GoogleBooksResponse

}

class GoogleBooksServiceImpl(private val client: HttpClient) : GoogleBooksService {
    override suspend fun search(query: String): GoogleBooksResponse {
        return client.get(Url("https://www.googleapis.com/books/v1/volumes")) {
            parameter("q", query)
        }
    }

    override suspend fun searchBooks(query: String): GoogleBooksResponse {
        return client.get(Url("https://www.googleapis.com/books/v1/volumes")) {
            parameter("q", query)
        }
    }
}

