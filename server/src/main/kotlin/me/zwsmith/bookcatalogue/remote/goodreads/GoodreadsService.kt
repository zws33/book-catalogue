package me.zwsmith.bookcatalogue.remote.goodreads

import io.ktor.client.HttpClient
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.http.URLProtocol
import io.ktor.util.url
import me.zwsmith.bookcatalogue.models.Book
import org.simpleframework.xml.Serializer
import org.simpleframework.xml.core.Persister

@Suppress("FunctionName")
fun GoodreadsService(client: HttpClient, apiKey: String): GoodreadsService =
    GoodreadsServiceImpl(client, apiKey)

interface GoodreadsService {
    suspend fun search(query: String): GoodreadsSearchResponse
    suspend fun searchAuthors(author: String): GoodreadsAuthorResponse
    suspend fun searchBooks(query: String): List<Book>
}

class GoodreadsServiceImpl(
    private val client: HttpClient,
    private val apiKey: String,
    private val serializer: Serializer = Persister()
) : GoodreadsService {

    override suspend fun search(query: String): GoodreadsSearchResponse {
        return get(path = SEARCH) {
            parameter("key", apiKey)
            parameter("q", query)
        }.let(serializer::parseXml)
    }

    override suspend fun searchBooks(query: String): List<Book> {
        return search(query)
            .search
            ?.results
            ?.mapNotNull { it.book?.toBook() } ?: emptyList()
    }

    override suspend fun searchAuthors(author: String): GoodreadsAuthorResponse {
        val responseString = client.get<String>(
            urlString = url {
                protocol = URLProtocol.HTTPS
                host = HOST
                path("api",
                    AUTHOR_URL, author)
            }
        ) {
            parameter("key", apiKey)
        }
        return responseString.let(serializer::parseXml)
    }

    private suspend fun get(path: String, block: HttpRequestBuilder.() -> Unit = {}): String {
        return client.get(scheme = SCHEME, host = HOST, path = path, block = block)
    }
    companion object {
        private const val AUTHOR_URL = "author_url"
        private const val SEARCH: String = "/search.xml"
        private const val SCHEME: String = "https"
        private const val HOST: String = "www.goodreads.com"
    }
}

inline fun <reified T> Serializer.parseXml(xmlString: String): T {
    return read(T::class.java, xmlString)
}