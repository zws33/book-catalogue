package me.zwsmith.bookcatalogue.database.tables

import me.zwsmith.bookcatalogue.models.Book
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.statements.InsertStatement

object BooksTable : IntIdTable("books") {
    val bookId = varchar("bookId", 255).uniqueIndex()
    val title = varchar("title", 255)
    val imageUrl = varchar("imageUrl", 255)
    val authors = varchar("authors", 255)
    val smallImageUrl = varchar("smallImageUrl", 255)
}

fun BooksTable.insert(book: Book): InsertStatement<Number> {
    return insert { insertStatement: InsertStatement<Number> ->
        insertStatement[bookId] = book.id
        insertStatement[title] = book.title
        insertStatement[authors] = book.authors.joinToString()
        insertStatement[imageUrl] = book.imageUrl
        insertStatement[smallImageUrl] = book.smallImageUrl
    }
}

fun ResultRow.toBook(): Book {
    return Book(
        id = this[BooksTable.bookId],
        title = this[BooksTable.title],
        imageUrl = this[BooksTable.imageUrl],
        authors = this[BooksTable.authors].split(", "),
        smallImageUrl = this[BooksTable.smallImageUrl]
    )
}