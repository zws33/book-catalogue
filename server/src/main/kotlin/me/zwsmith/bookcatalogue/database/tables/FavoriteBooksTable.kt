package me.zwsmith.bookcatalogue.database.tables

import org.jetbrains.exposed.dao.IntIdTable

object FavoriteBooksTable : IntIdTable("favoriteBooks") {
    val userId = integer("userId")
    val bookId = varchar("bookId", 255).uniqueIndex()
}