package me.zwsmith.bookcatalogue.database

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import me.zwsmith.bookcatalogue.database.tables.*
import me.zwsmith.bookcatalogue.models.Book
import me.zwsmith.bookcatalogue.models.User
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction

interface BookCatalogueDatabase {
    fun init(credentials: MysqlCredentials? = null): BookCatalogueDatabase
    suspend fun <T> withTransaction(block: suspend () -> T): T
    fun hikari(credentials: MysqlCredentials?): HikariDataSource

    suspend fun createUser(username: String, password: String): Int
    suspend fun findUserByid(id: Int): User?
    suspend fun findUserByUsername(username: String): User?

    suspend fun getFavoriteBooksForUser(userId: Int): List<Book>
    suspend fun addFavoriteBookForUser(userId: Int, bookId: String)

    suspend fun addBook(book: Book)
    suspend fun getBookById(bookId: String): Book?
}

object BookCatalogueDatabaseImpl : BookCatalogueDatabase {
    private const val mysqlUrl = "jdbc:mysql://localhost:3306/book_catalogue"
    private const val mysqlDriver = "com.mysql.cj.jdbc.Driver"
    private const val h2Url = "jdbc:h2:mem:test"
    private const val h2Driver = "org.h2.Driver"

    override fun init(credentials: MysqlCredentials?): BookCatalogueDatabase {
        Database.connect(hikari(credentials))
        transaction {
            SchemaUtils.create(UsersTable)
            SchemaUtils.create(BooksTable)
            SchemaUtils.create(FavoriteBooksTable)
        }
        return this
    }

    override fun hikari(credentials: MysqlCredentials?): HikariDataSource {
        return HikariConfig().apply {
            if (credentials != null) {
                jdbcUrl = mysqlUrl
                driverClassName = mysqlDriver
                username = credentials.username
                password = credentials.password
            } else {
                jdbcUrl = h2Url
                driverClassName = h2Driver
            }
        }.let(::HikariDataSource)
    }

    override suspend fun <T> withTransaction(block: suspend () -> T): T = newSuspendedTransaction { block() }

    override suspend fun addFavoriteBookForUser(userId: Int, bookId: String) {
        withTransaction {
            FavoriteBooksTable.insert { insertStatement ->
                insertStatement[this.userId] = userId
                insertStatement[this.bookId] = bookId
            }
        }
    }

    override suspend fun getFavoriteBooksForUser(userId: Int): List<Book> {
        return withTransaction {
            BooksTable.join(
                otherTable = FavoriteBooksTable,
                joinType = JoinType.INNER,
                onColumn = BooksTable.bookId,
                otherColumn = FavoriteBooksTable.bookId
            )
                .select { (BooksTable.bookId eq FavoriteBooksTable.bookId) and (FavoriteBooksTable.userId eq userId) }
                .map { it.toBook() }
        }
    }

    override suspend fun addBook(book: Book) {
        withTransaction {
            BooksTable.insert(book)
        }
    }

    override suspend fun getBookById(bookId: String): Book? {
        return withTransaction {
            BooksTable.select { BooksTable.bookId eq bookId }.map { it.toBook() }.firstOrNull()
        }
    }

    override suspend fun createUser(username: String, password: String): Int {
        exposedLogger.debug("BookCatalogueDatabase: username = $username")
        return withTransaction {
            UsersTable.insertAndGetId {
                it[this.name] = username
                it[this.password] = password
            }.value
        }
    }

    override suspend fun findUserByUsername(username: String): User? {
        return withTransaction {
            UsersTable.select { UsersTable.name eq username }
                .singleOrNull()
                ?.toUser()
        }
    }

    override suspend fun findUserByid(id: Int): User? {
        return withTransaction {
            UsersTable.select { UsersTable.id eq id }
                .singleOrNull()
                ?.toUser()
        }
    }
}

private fun ResultRow.toBook(): Book {
    return Book(
        id = this[BooksTable.bookId],
        title = this[BooksTable.title],
        imageUrl = this[BooksTable.imageUrl],
        authors = this[BooksTable.authors].split(", "),
        smallImageUrl = this[BooksTable.smallImageUrl]
    )
}
