package me.zwsmith.bookcatalogue.database

data class MysqlCredentials(val username: String, val password: String)