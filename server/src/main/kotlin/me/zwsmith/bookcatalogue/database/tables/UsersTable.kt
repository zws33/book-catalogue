package me.zwsmith.bookcatalogue.database.tables

import me.zwsmith.bookcatalogue.models.User
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ResultRow

object UsersTable : IntIdTable("users") {
    val name = varchar("name", 255)
    val password = varchar("password", 255)
}

fun ResultRow.toUser(): User {
    return User(
        id = this[UsersTable.id].value,
        name = this[UsersTable.name],
        password = this[UsersTable.password]
    )
}