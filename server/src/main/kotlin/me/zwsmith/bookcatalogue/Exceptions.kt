package me.zwsmith.bookcatalogue

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()