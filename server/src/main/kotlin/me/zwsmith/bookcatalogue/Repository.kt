package me.zwsmith.bookcatalogue

import io.ktor.auth.UserPasswordCredential
import me.zwsmith.bookcatalogue.database.BookCatalogueDatabase
import me.zwsmith.bookcatalogue.models.Author
import me.zwsmith.bookcatalogue.models.Book
import me.zwsmith.bookcatalogue.models.User
import me.zwsmith.bookcatalogue.remote.goodreads.GoodreadsService
import me.zwsmith.bookcatalogue.remote.goodreads.toAuthor
import me.zwsmith.bookcatalogue.remote.google.GoogleBooksService
import me.zwsmith.bookcatalogue.remote.google.toBook
import org.jetbrains.exposed.sql.exposedLogger


interface Repository {
    suspend fun findUserByCredentials(userPasswordCredential: UserPasswordCredential): User?
    suspend fun createUser(username: String, password: String): Int?
    suspend fun addBook(book: Book)
    suspend fun searchBooks(query: String): List<Book>
    suspend fun searchAuthors(query: String): Author?
    suspend fun getFavoriteBooksForUser(userId: Int): List<Book>
    suspend fun findUserById(id: Int): User?
    suspend fun searchGoogle(query: String): List<Book>
    suspend fun addFavoriteBookForUser(userId: Int, bookId: String)
}

@Suppress("FunctionName")
fun Repository(
    encryptionUtil: EncryptionUtil,
    database: BookCatalogueDatabase,
    goodreadsService: GoodreadsService,
    googleBooksService: GoogleBooksService
): Repository {
    return RepositoryImpl(encryptionUtil, database, goodreadsService, googleBooksService)
}

class RepositoryImpl(
    private val encryptionUtil: EncryptionUtil,
    private val database: BookCatalogueDatabase,
    private val goodreadsService: GoodreadsService,
    private val googleBooksService: GoogleBooksService
) : Repository {

    override suspend fun searchGoogle(query: String): List<Book> {
        return googleBooksService.search(query).items.map { it.toBook() }
    }

    override suspend fun findUserById(id: Int): User? {
        return database.findUserByid(id)
    }

    override suspend fun findUserByCredentials(userPasswordCredential: UserPasswordCredential): User? {
        return database.findUserByUsername(userPasswordCredential.name)
            ?.takeIf { userPasswordCredential.password.matches(it.password) }
    }

    private fun String.matches(hashed: String) = encryptionUtil.checkPassword(this, hashed)

    override suspend fun createUser(username: String, password: String): Int? {
        exposedLogger.debug("Repository: username = $username")
        return password.hashPassword()?.let {
            database.createUser(username, it)
        }
    }

    override suspend fun searchBooks(query: String): List<Book> {
        return TODO()
    }

    override suspend fun searchAuthors(query: String): Author? {
        return goodreadsService.searchAuthors(query).author?.toAuthor()
    }

    override suspend fun getFavoriteBooksForUser(userId: Int): List<Book> {
        return database.getFavoriteBooksForUser(userId)
    }

    override suspend fun addBook(book: Book) {
        database.addBook(book)
    }

    override suspend fun addFavoriteBookForUser(userId: Int, bookId: String) {
        database.addFavoriteBookForUser(userId, bookId)
    }

    private fun String.hashPassword(): String? = encryptionUtil.hashPassword(this)
}

