package me.zwsmith.bookcatalogue

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import me.zwsmith.bookcatalogue.models.User
import java.util.*


interface JwtConfig {

    val verifier: JWTVerifier

    /**
     * Produce a token for this combination of me.zwsmith.book_catalogue.User and Account
     */
    fun makeToken(user: User): String

}

@Suppress("FunctionName")
fun JwtConfig(secret: String): JwtConfig = JwtConfigImpl(secret)

class JwtConfigImpl(
    secret: String
) : JwtConfig {

    private val algorithm = Algorithm.HMAC512(secret)

    override val verifier: JWTVerifier = JWT
        .require(algorithm)
        .withIssuer(issuer)
        .build()

    /**
     * Produce a token for this combination of me.zwsmith.book_catalogue.User and Account
     */
    override fun makeToken(user: User): String = JWT.create()
        .withSubject("Authentication")
        .withIssuer(issuer)
        .withClaim("id", user.id)
        .withExpiresAt(getExpiration())
        .sign(algorithm)

    /**
     * Calculate the expiration Date based on current time + the given validity
     */
    private fun getExpiration() = Date(System.currentTimeMillis() + validityInMs)

    companion object {
        private const val issuer = "ktor.io"
        private const val validityInMs = 36_000_00 * 10 // 10 hours
    }
}

