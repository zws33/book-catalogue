package me.zwsmith.bookcatalogue.routes

data class AddFavoriteBookRequest(
    val bookId: String
)