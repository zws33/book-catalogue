package me.zwsmith.bookcatalogue.routes

data class RegisterUserResponse(
    val userId: Int
)