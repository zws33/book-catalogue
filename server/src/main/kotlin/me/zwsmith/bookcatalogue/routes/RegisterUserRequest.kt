package me.zwsmith.bookcatalogue.routes

data class RegisterUserRequest(
    val username: String,
    val password: String
)