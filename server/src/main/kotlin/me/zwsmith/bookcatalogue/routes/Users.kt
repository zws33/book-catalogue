package me.zwsmith.bookcatalogue.routes

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.util.pipeline.PipelineContext
import me.zwsmith.bookcatalogue.Repository

fun Routing.users(repository: Repository) {
    post("users/register") {
        val credentials: RegisterUserRequest = call.receive()
        val userId: Int? = repository.createUser(credentials.username, credentials.password)
        userId?.let {
            call.respond(HttpStatusCode.OK, RegisterUserResponse(userId))
        } ?: call.respond(HttpStatusCode.InternalServerError, "Could not create user")
    }

    route("users/{id}") {
        route("books") {
            post("favorites") {
                val id: Int? = userId()
                val request: AddFavoriteBookRequest = call.receive()
                if (id != null) {
                    repository.addFavoriteBookForUser(id, request.bookId)
                    call.respond(HttpStatusCode.OK)
                } else {
                    call.respond(HttpStatusCode.BadRequest, "id was null")
                }
            }
            get("favorites") {
                val id: Int? = userId()
                if (id != null) {
                    call.respond(HttpStatusCode.OK, repository.getFavoriteBooksForUser(id))
                }
            }
        }
    }
}

private fun PipelineContext<Unit, ApplicationCall>.userId(): Int? = call.parameters[ID]?.toInt()
private const val ID = "id"

