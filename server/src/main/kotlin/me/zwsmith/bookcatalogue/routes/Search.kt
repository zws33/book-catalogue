package me.zwsmith.bookcatalogue.routes

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.route
import me.zwsmith.bookcatalogue.Repository
import me.zwsmith.bookcatalogue.models.Author
import me.zwsmith.bookcatalogue.models.Book

fun Routing.search(
    repository: Repository
) {
    route("/search") {
        get("/books") {
            val query: String? = call.request.queryParameters["q"]
            if (query.isNullOrBlank()) {
                call.respond(HttpStatusCode.UnprocessableEntity)
            } else {
                val books: List<Book> = repository.searchGoogle(query)
                call.respond(books)
            }
        }

        get("/authors/{authorQuery}") {
            val authorQuery: String? = call.parameters["authorQuery"]
            val author: Author? = authorQuery?.let { author -> repository.searchAuthors(author) }
            call.respond(author ?: HttpStatusCode.NotFound)
        }
    }
}