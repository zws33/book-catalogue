package me.zwsmith.bookcatalogue.routes

import io.ktor.application.call
import io.ktor.auth.UserPasswordCredential
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.post
import me.zwsmith.bookcatalogue.JwtConfig
import me.zwsmith.bookcatalogue.Repository

fun Routing.login(repository: Repository, jwtConfig: JwtConfig) {
    post("login") {
        val credentials = call.receive<UserPasswordCredential>()
        val user = repository.findUserByCredentials(credentials)
        if (user != null) {
            call.respondText(jwtConfig.makeToken(user))
        } else {
            call.respond(
                HttpStatusCode.NotFound,
                "Could not find account with those credentials"
            )
        }
    }
}