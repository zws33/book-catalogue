package me.zwsmith.bookcatalogue

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.features.StatusPages
import io.ktor.gson.gson
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import me.zwsmith.bookcatalogue.routes.login
import me.zwsmith.bookcatalogue.routes.search
import me.zwsmith.bookcatalogue.routes.users

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    initializeGraph().run {
        install(CallLogging)
        install(DefaultHeaders) {
            header("X-Engine", "Ktor") // will send this header with each response
        }
        install(ContentNegotiation) {
            gson()
        }
        install(StatusPages) {
            exception<AuthenticationException> { cause ->
                call.respond(HttpStatusCode.Unauthorized)
            }
            exception<AuthorizationException> { cause ->
                call.respond(HttpStatusCode.Forbidden)
            }
        }

        install(Routing) {
            get("/") {
                call.respond(HttpStatusCode.OK, "Hello World!")
            }
            search(repository)
            login(repository, jwtConfig)
            users(repository)
        }
    }
}