package me.zwsmith.bookcatalogue

import io.ktor.application.Application
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.features.logging.DEFAULT
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logger
import io.ktor.client.features.logging.Logging
import me.zwsmith.bookcatalogue.database.BookCatalogueDatabase
import me.zwsmith.bookcatalogue.database.BookCatalogueDatabaseImpl
import me.zwsmith.bookcatalogue.remote.goodreads.GoodreadsService
import me.zwsmith.bookcatalogue.remote.google.GoogleBooksService

fun Application.initializeGraph(): Graph = GraphImpl(this)

interface Graph {
    val goodreadsService: GoodreadsService
    val repository: Repository
    val jwtConfig: JwtConfig
    val encryptionUtil: EncryptionUtil
    val database: BookCatalogueDatabase
    val googleBooksService: GoogleBooksService
}

@Suppress("FunctionName")
class GraphImpl(private val application: Application) : Graph {
    override val encryptionUtil: EncryptionUtil = EncryptionUtil()
    override val repository: Repository by lazy {
        Repository(encryptionUtil, database, goodreadsService, googleBooksService)
    }

    override val googleBooksService: GoogleBooksService by lazy {
        GoogleBooksService(client)
    }

    override val jwtConfig by lazy {
        JwtConfig(application.getEnvironmentVariable(SECRET))
    }

    override val goodreadsService by lazy {
        GoodreadsService(client, apiKey)
    }

    private val client by lazy {
        Client()
    }

    private val apiKey by lazy {
        application.getEnvironmentVariable(GOODREADS_API_KEY)
    }

    override val database: BookCatalogueDatabase by lazy {
        val username = application.getEnvironmentVariable(MYSQL_USERNAME)
        val password = application.getEnvironmentVariable(MYSQL_PASSWORD)
        BookCatalogueDatabaseImpl.init()
    }

    private fun Client() = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
        install(Logging) {
            logger = Logger.DEFAULT
            level = LogLevel.INFO
        }
    }

    companion object {
        private const val SECRET = "ktor.secret"
        private const val GOODREADS_API_KEY = "ktor.goodreadsApiKey"
        private const val MYSQL_USERNAME = "ktor.mysql.username"
        private const val MYSQL_PASSWORD = "ktor.mysql.password"
    }
}