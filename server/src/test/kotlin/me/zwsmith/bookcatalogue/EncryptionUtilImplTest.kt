package me.zwsmith.bookcatalogue

import org.junit.Test
import kotlin.test.assertTrue
import kotlin.test.fail


internal class EncryptionUtilImplTest {

    @Test
    fun genSalt() {
    }

    @Test
    fun hashPassword() {
    }

    @Test
    fun checkPassword() {
        // Given
        val encryptionUtil = EncryptionUtilImpl
        val password = "password"

        // When
        val hashPassword = encryptionUtil.hashPassword(password) ?: fail("Error hashing password")

        // Then
        assertTrue(encryptionUtil.checkPassword(password, hashPassword))
    }
}