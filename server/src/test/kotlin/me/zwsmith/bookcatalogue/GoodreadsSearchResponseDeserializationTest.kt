package me.zwsmith.bookcatalogue

import me.zwsmith.bookcatalogue.remote.goodreads.GoodreadsSearchResponse
import me.zwsmith.bookcatalogue.remote.goodreads.parseXml
import org.junit.Test
import org.simpleframework.xml.core.Persister
import kotlin.test.assertEquals

class GoodreadsSearchResponseDeserializationTest {
    @Test
    fun parseXmlReturnsExpectedResponse() {
        val serializer = Persister()
        val parsedResponse = serializer.parseXml<GoodreadsSearchResponse>(fakeXmlResponse)
        assertEquals(expectedResponse, parsedResponse)
    }
}

val fakeXmlResponse = """
<?xml version="1.0" encoding="UTF-8"?>
<GoodreadsResponse>
    |<Request>
        |<authentication>true</authentication>
        |<key><![CDATA[YLRzTq0bHeQoBb4RioxA]]></key>
        |<method><![CDATA[search_index]]></method>
    |</Request>
    |<search>
        |<query><![CDATA[brandon sanderson]]></query>
        |<results-start>1</results-start>
        |<results-end>20</results-end>
        |<total-results>294</total-results>
        |<source>Goodreads</source>
        |<query-time-seconds>0.22</query-time-seconds>
        |<results>
            |<work>
                |<id type="integer">66322</id>
                |<books_count type="integer">103</books_count>
                |<ratings_count type="integer">320752</ratings_count>
                |<text_reviews_count type="integer">21968</text_reviews_count>
                |<original_publication_year type="integer">2006</original_publication_year>
                |<original_publication_month type="integer">7</original_publication_month>
                |<original_publication_day type="integer">17</original_publication_day>
                |<average_rating>4.45</average_rating>
                |<best_book type="Book">
                    |<id type="integer">68428</id>
                    |<title>The Final Empire (Mistborn, #1)</title>
                    |<author>
                        |<id type="integer">38550</id>
                        |<name>Brandon Sanderson</name>
                    |</author>
                    |<image_url>https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1480717416i/68428._SX98_.jpg</image_url>
                    |<small_image_url>https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1480717416i/68428._SY75_.jpg</small_image_url>
                |</best_book>
            |</work>
            |<work>
                |<id type="integer">8134945</id>
                |<books_count type="integer">61</books_count>
                |<ratings_count type="integer">235096</ratings_count>
                |<text_reviews_count type="integer">15623</text_reviews_count>
                |<original_publication_year type="integer">2010</original_publication_year>
                |<original_publication_month type="integer">8</original_publication_month>
                |<original_publication_day type="integer">31</original_publication_day>
                |<average_rating>4.65</average_rating>
                |<best_book type="Book">
                    |<id type="integer">7235533</id>
                    |<title>The Way of Kings (The Stormlight Archive, #1)</title>
                    |<author>
                        |<id type="integer">38550</id>
                        |<name>Brandon Sanderson</name>
                    |</author>
                    |<image_url>https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1388184640i/7235533._SX98_.jpg</image_url>
                    |<small_image_url>https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1388184640i/7235533._SY75_.jpg</small_image_url>
                |</best_book>
            |</work>
        |</results>
    |</search>
</GoodreadsResponse>
|""".trimMargin()

val expectedResponse = GoodreadsSearchResponse(
    search = GoodreadsSearchResponse.Search(
        mutableListOf(
            GoodreadsSearchResponse.Work(
                id = 66322,
                booksCount = 103,
                ratingsCount = 320752,
                textReviewsCount = 21968,
                originalPublicationYear = 2006,
                originalPublicationMonth = 7,
                originalPublicationDay = 17,
                averageRating = 4.45,
                book = GoodreadsSearchResponse.Book(
                    id = 68428,
                    title = "The Final Empire (Mistborn, #1)",
                    imageUrl = "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1480717416i/68428._SX98_.jpg",
                    author = GoodreadsSearchResponse.Author(
                        id = 38550,
                        name = "Brandon Sanderson"
                    ),
                    smallImageUrl = "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1480717416i/68428._SY75_.jpg"
                )
            ),
            GoodreadsSearchResponse.Work(
                id = 8134945,
                booksCount = 61,
                ratingsCount = 235096,
                textReviewsCount = 15623,
                originalPublicationYear = 2010,
                originalPublicationMonth = 8,
                originalPublicationDay = 31,
                averageRating = 4.65,
                book = GoodreadsSearchResponse.Book(
                    id = 7235533,
                    title = "The Way of Kings (The Stormlight Archive, #1)",
                    imageUrl = "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1388184640i/7235533._SX98_.jpg",
                    author = GoodreadsSearchResponse.Author(
                        id = 38550,
                        name = "Brandon Sanderson"
                    ),
                    smallImageUrl = "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1388184640i/7235533._SY75_.jpg"
                )
            )
        )
    )
)