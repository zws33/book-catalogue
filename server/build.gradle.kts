import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val kotlin_version: String by project
val ktor_version: String by project
val logback_version: String by project
val kotlin_css_jvm_version: String by project
val h2_version: String by project
val exposed_version: String by project
val hikari_cp_version: String by project
val jbcrypt_version: String by project
val simple_xml_version: String by project
val kluent_version: String by project

plugins {
    kotlin("jvm")
    id("kotlinx-serialization")
    application
    id("com.github.johnrengelman.shadow") version "5.0.0"
    kotlin("plugin.serialization")
}


application {
    mainClassName = "io.ktor.server.netty.EngineMain"
    version = "0.0.1"
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(project(":shared"))
    implementation("io.ktor:ktor-server-netty:$ktor_version")

    implementation("ch.qos.logback:logback-classic:$logback_version")

    // Ktor
    implementation("io.ktor:ktor-server-core:$ktor_version")
    implementation("io.ktor:ktor-server-host-common:$ktor_version")

    // HTML
    implementation("io.ktor:ktor-html-builder:$ktor_version")

    // Http Client
    implementation("io.ktor:ktor-client-core:$ktor_version")
    implementation("io.ktor:ktor-client-core-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-apache:$ktor_version")
    implementation("io.ktor:ktor-client-logging-jvm:$ktor_version")

    // Database
    implementation("com.h2database:h2:$h2_version")
    implementation("org.jetbrains.exposed:exposed:$exposed_version")
    implementation("com.zaxxer:HikariCP:$hikari_cp_version")
    implementation("mysql:mysql-connector-java:8.0.18")

    // TLS
    implementation("io.ktor:ktor-network-tls:$ktor_version")

    // Encryption
    implementation("org.mindrot:jbcrypt:$jbcrypt_version")

    // Serialization
    implementation("org.simpleframework:simple-xml:$simple_xml_version")
    implementation("io.ktor:ktor-gson:$ktor_version")
    implementation("io.ktor:ktor-client-serialization-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-cio:$ktor_version")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.14.0")

    // Authentication
    implementation("io.ktor:ktor-auth:$ktor_version")
    implementation("io.ktor:ktor-auth-jwt:$ktor_version")

    testImplementation("org.amshove.kluent:kluent:$kluent_version")
    testImplementation("io.ktor:ktor-server-tests:$ktor_version")
}


tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.freeCompilerArgs += "-Xuse-experimental=kotlin.Experimental"
}

tasks.withType<Jar> {
    manifest {
        attributes(
            mapOf(
                "Main-Class" to application.mainClassName
            )
        )
    }
}