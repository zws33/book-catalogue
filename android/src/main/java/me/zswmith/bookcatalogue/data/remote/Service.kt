package me.zswmith.bookcatalogue.data.remote

import me.zwsmith.bookcatalogue.models.Book
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface Service {
    @GET("search/books")
    suspend fun search(@Query("q") q: String): Response<List<Book>>
}