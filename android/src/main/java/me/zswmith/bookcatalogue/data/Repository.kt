package me.zswmith.bookcatalogue.data

import me.zswmith.bookcatalogue.data.remote.Service
import me.zwsmith.bookcatalogue.models.Book
import retrofit2.Response

interface Repository {
    suspend fun search(query: String): Response<List<Book>>
}

class RepositoryImpl(private val service: Service): Repository {
    override suspend fun search(query: String): Response<List<Book>> {
        return service.search(query)
    }
}