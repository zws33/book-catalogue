@file:Suppress("FunctionName")

package me.zswmith.bookcatalogue.di

import me.zswmith.bookcatalogue.data.Repository
import me.zswmith.bookcatalogue.data.RepositoryImpl
import me.zswmith.bookcatalogue.data.remote.Service
import me.zswmith.bookcatalogue.ui.search.SearchViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

val mainModule = module {
    single { HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY } }
    single { Client(get()) }
    single { Retrofit(get()) }
    single { Service(get()) }
    single<Repository> { RepositoryImpl(get()) }

    viewModel { SearchViewModel(get()) }
}

private fun Client(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor)
        .build()
}

private const val BASE_URL = "http://10.0.2.2:8080/"

fun Service(retrofit: Retrofit): Service {
    return retrofit.create()
}

fun Retrofit(client: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .baseUrl(BASE_URL)
        .build()
}