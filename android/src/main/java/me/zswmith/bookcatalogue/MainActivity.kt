package me.zswmith.bookcatalogue

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.android.R
import me.zswmith.bookcatalogue.ui.navigation.Destination
import me.zswmith.bookcatalogue.ui.navigation.NavigationHost
import me.zswmith.bookcatalogue.ui.navigation.toFragment
import me.zswmith.bookcatalogue.ui.search.SearchFragment

class MainActivity : AppCompatActivity(), NavigationHost {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            val fragment = SearchFragment.newInstance()
            navigateTo(fragment)
        }
    }

    private fun navigateTo(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .addToBackStack(fragment::class.java.canonicalName)
            .replace(R.id.container, fragment)
            .commit()
    }

    override fun navigateTo(destination: Destination) {
        navigateTo(destination.toFragment())
    }
}