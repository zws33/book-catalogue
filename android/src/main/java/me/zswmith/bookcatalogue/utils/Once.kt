package me.zswmith.bookcatalogue.utils

import kotlin.reflect.KProperty

class Once<T>(private val content: T) {
    private var hasBeenHandled = false

    operator fun getValue(thisRef: Any?, prop: KProperty<*>): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }
}