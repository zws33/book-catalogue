package me.zswmith.bookcatalogue.ui.utils

import android.text.Editable
import android.text.TextWatcher

@Suppress("FunctionName")
fun OnTextChangedWatcher(onTextChanged: (text: CharSequence?) -> Unit): TextWatcher {
    return object : TextWatcher {
        override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
            onTextChanged(text)
        }

        override fun afterTextChanged(p0: Editable?) {
            // No-op
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            // No-op
        }

    }
}