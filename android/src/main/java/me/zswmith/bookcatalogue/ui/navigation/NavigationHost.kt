package me.zswmith.bookcatalogue.ui.navigation

interface NavigationHost {
    fun navigateTo(destination: Destination)
}