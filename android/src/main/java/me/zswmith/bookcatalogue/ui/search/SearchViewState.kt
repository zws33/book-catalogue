package me.zswmith.bookcatalogue.ui.search

data class SearchViewState(
    val showError: Boolean,
    val showProgress: Boolean,
    val searchResults: List<BookListItem>
) {
    companion object {
        val Loading: SearchViewState
            get() = SearchViewState(
                showError = false,
                showProgress = true,
                searchResults = emptyList()
            )
        val Error: SearchViewState
            get() = SearchViewState(
                showError = true,
                showProgress = false,
                searchResults = emptyList()
            )
    }
}

data class BookListItem(
    val title: String,
    val author: String,
    val thumbnailUrl: String,
    val onClick: () -> Unit
)