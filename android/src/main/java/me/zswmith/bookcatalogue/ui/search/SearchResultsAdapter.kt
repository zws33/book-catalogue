package me.zswmith.bookcatalogue.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.android.R
import me.zswmith.bookcatalogue.ui.utils.load

class SearchResultsAdapter() : RecyclerView.Adapter<SearchResultsAdapter.SearchResult>() {
    private var bookList: List<BookListItem> = emptyList()


    fun setData(bookList: List<BookListItem>) {
        this.bookList = bookList
        notifyDataSetChanged()
    }

    class SearchResult(view: View) : RecyclerView.ViewHolder(view) {
        val titleView: TextView = view.findViewById(R.id.title)
        val authorView: TextView = view.findViewById(R.id.author)
        val thumbnail: ImageView = view.findViewById(R.id.book_thumbnail)
        lateinit var onClick: () -> Unit
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchResult {
        val view = parent.inflate(R.layout.book_list_item)
        return SearchResult(view).also { searchResult -> view.setOnClickListener { searchResult.onClick() } }
    }

    private fun ViewGroup.inflate(layout: Int): View {
        return LayoutInflater.from(context).inflate(layout, this, false)
    }

    override fun onBindViewHolder(holder: SearchResult, position: Int) {
        val bookListItem = bookList[position]
        holder.titleView.text = bookListItem.title
        holder.thumbnail.load(bookListItem.thumbnailUrl)
        holder.onClick = bookListItem.onClick
    }

    override fun getItemCount() = bookList.size
}