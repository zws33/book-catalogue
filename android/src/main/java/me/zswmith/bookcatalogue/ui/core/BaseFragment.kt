package me.zswmith.bookcatalogue.ui.core

import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import me.zswmith.bookcatalogue.ui.navigation.Destination
import me.zswmith.bookcatalogue.ui.navigation.NavigationHost

abstract class BaseFragment : Fragment() {
    fun <T> LiveData<T>.observe(onChanged: (T) -> Unit) {
        observe(this@BaseFragment, Observer(onChanged))
    }

    fun navigateTo(destination: Destination) {
        navigationHost?.navigateTo(destination)
    }

    protected fun showErrorDialog() {
        val dialog = AlertDialog.Builder(context!!)
            .setCancelable(false)
            .setPositiveButton("Ok") { dialogInterface, _ -> dialogInterface.cancel() }
            .setTitle("Oops, something went wrong!")
            .create()

        dialog.show()
    }

    private val navigationHost: NavigationHost? get() = activity as? NavigationHost

}