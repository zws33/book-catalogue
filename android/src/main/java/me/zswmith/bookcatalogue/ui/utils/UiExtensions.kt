package me.zswmith.bookcatalogue.ui.utils

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

fun EditText.onTextChanged(onTextChanged: (text: CharSequence?) -> Unit) {
    addTextChangedListener(OnTextChangedWatcher(onTextChanged))
}

fun ImageView.load(url: String) {
    Picasso.get()
        .load(url)
        .fit()
        .into(this)
}

@SuppressLint("WrongConstant")
fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun EditText.onDoneClicked(block: () -> Unit) {
    this.setOnEditorActionListener { _, actionId, _ ->
        when (actionId) {
            EditorInfo.IME_ACTION_DONE -> {
                block()
                hideKeyboard()
                true
            }
            else -> false
        }
    }
}

fun TextView.setTextOrHide(text: String?) {
    if(text != null) {
        this.text = text
        visibility = View.VISIBLE
    } else {
        visibility = View.GONE
    }
}