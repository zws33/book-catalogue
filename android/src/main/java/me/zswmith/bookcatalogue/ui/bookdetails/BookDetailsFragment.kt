package me.zswmith.bookcatalogue.ui.bookdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.android.R
import me.zswmith.bookcatalogue.ui.core.BaseFragment
import me.zswmith.bookcatalogue.ui.utils.load
import me.zswmith.bookcatalogue.ui.utils.setTextOrHide

class BookDetailsFragment : BaseFragment() {
    private lateinit var bookImage: ImageView
    private lateinit var bookTitle: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.book_details_fragment, container, false).also(::initUi)
    }

    private fun initUi(view: View) {
        bookImage = view.findViewById(R.id.book_image)
        bookTitle = view.findViewById(R.id.book_title)
        bookTitle.setTextOrHide(arguments?.getString(TITLE))
        arguments?.getString(IMAGE_URL)?.let { bookImage.load(it) }
    }

    companion object {
        fun newInstance(title: String, imageUrl: String): BookDetailsFragment {
            val args = Bundle().apply {
                putString(TITLE, title)
                putString(IMAGE_URL, imageUrl)
            }
            return BookDetailsFragment().apply {
                arguments = args
            }
        }

        const val IMAGE_URL = "IMAGE_URL"
        const val TITLE = "TITLE"
    }
}