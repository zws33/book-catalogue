package me.zswmith.bookcatalogue.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import me.zswmith.bookcatalogue.data.Repository
import me.zswmith.bookcatalogue.ui.navigation.Destination
import me.zswmith.bookcatalogue.ui.navigation.NavigationEvent
import me.zwsmith.bookcatalogue.models.Book
import retrofit2.Response

class SearchViewModel(
    private val repository: Repository
) : ViewModel() {
    val viewStates: LiveData<SearchViewState> get() = _viewStates
    private val _viewStates: MutableLiveData<SearchViewState> = MutableLiveData()

    val navigationEvents: LiveData<NavigationEvent> get() = _destinations
    private val _destinations: MutableLiveData<NavigationEvent> = MutableLiveData()

    fun search(searchValue: String) {
        viewModelScope.launch {
            _viewStates.value = SearchViewState.Loading
            val response: Response<List<Book>> = withContext(Dispatchers.IO) {
                repository.search(searchValue)
            }
            _viewStates.value = if (response.isSuccessful) {
                SearchViewState(
                    showError = false,
                    showProgress = false,
                    searchResults = response.body()?.map(::toBookListItem) ?: emptyList()
                )
            } else {
                SearchViewState.Error
            }
        }
    }

    private fun toBookListItem(book: Book): BookListItem {
        return with(book) {
            BookListItem(
                title = title,
                author = authors.toString(),
                thumbnailUrl = smallImageUrl,
                onClick = { _destinations.value = NavigationEvent(Destination.BookDetails(title, imageUrl)) }
            )
        }
    }
}

