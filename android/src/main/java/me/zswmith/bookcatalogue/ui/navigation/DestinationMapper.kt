package me.zswmith.bookcatalogue.ui.navigation

import androidx.fragment.app.Fragment
import me.zswmith.bookcatalogue.ui.bookdetails.BookDetailsFragment
import me.zswmith.bookcatalogue.ui.search.SearchFragment

fun Destination.toFragment(): Fragment {
    return when(this) {
        is Destination.Main -> SearchFragment.newInstance()
        is Destination.BookDetails -> BookDetailsFragment.newInstance(title, imageUrl)
        Destination.Favorites -> TODO()
    }
}