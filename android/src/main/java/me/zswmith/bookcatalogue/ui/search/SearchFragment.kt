package me.zswmith.bookcatalogue.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.example.android.R
import me.zswmith.bookcatalogue.ui.core.BaseFragment
import me.zswmith.bookcatalogue.ui.utils.hideKeyboard
import me.zswmith.bookcatalogue.ui.utils.onDoneClicked
import me.zswmith.bookcatalogue.ui.utils.onTextChanged
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchFragment : BaseFragment() {

    companion object {
        fun newInstance() = SearchFragment()
    }

    private val viewModel: SearchViewModel by viewModel()
    private lateinit var searchButton: Button
    private lateinit var searchEditText: EditText
    private lateinit var searchResults: RecyclerView
    private lateinit var progressBar: ProgressBar
    private val searchResultsAdapter = SearchResultsAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.search_fragment, container, false).also { view -> initUi(view) }
    }

    private fun initUi(view: View) {
        searchButton = view.findViewById<Button>(R.id.search_button).also { button ->
            button.setOnClickListener { buttonView ->
                submitSearchQuery()
                buttonView.hideKeyboard()
            }
            button.isEnabled = false
        }
        searchEditText = view.findViewById<EditText>(R.id.search_edit_text).also { editText ->
            editText.onTextChanged { text -> searchButton.isEnabled = !text.isNullOrBlank() }
            editText.onDoneClicked(::submitSearchQuery)
        }
        searchResults = view.findViewById(R.id.search_results)
        searchResults.adapter = searchResultsAdapter
        progressBar = view.findViewById(R.id.progress_bar)
    }

    private fun submitSearchQuery() {
        searchEditText.text.toString().let { viewModel.search(it) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.viewStates.observe { viewState -> updateUi(viewState) }
        viewModel.navigationEvents.observe { event ->
            event.destination.ifNotNull { navigateTo(it) }
        }
    }

    private fun updateUi(viewState: SearchViewState) {
        if (viewState.showError) {
            showErrorDialog()
        }
        progressBar.isVisible = viewState.showProgress
        searchResultsAdapter.setData(viewState.searchResults)
    }
}

inline fun <reified T> T?.ifNotNull(block: (T) -> Unit) {
    if(this != null) {
        block(this)
    }
}
