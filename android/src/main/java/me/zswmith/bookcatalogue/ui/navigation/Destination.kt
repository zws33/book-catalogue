package me.zswmith.bookcatalogue.ui.navigation

import me.zswmith.bookcatalogue.utils.Once

sealed class Destination {
    object Main : Destination()
    data class BookDetails(
        val title: String,
        val imageUrl: String
    ) : Destination()

    object Favorites : Destination()
}


class NavigationEvent(destination: Destination) {
    val destination: Destination? by Once(destination)
}