import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
}

android {
    compileSdkVersion(29)
    buildToolsVersion = "29.0.2"

    defaultConfig {
        applicationId = "me.zswmith.bookcatalogue"
        minSdkVersion(21)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "1.0"
    }

    packagingOptions {
        exclude("META-INF/*.kotlin_module")
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(kotlin("stdlib"))

    implementation(project(":shared"))
    implementation("androidx.appcompat:appcompat:1.1.0")

    implementation("androidx.appcompat:appcompat:1.1.0")
    implementation("androidx.fragment:fragment:1.2.0")
    implementation("androidx.fragment:fragment-ktx:1.2.0")
    implementation("androidx.lifecycle:lifecycle-extensions:2.2.0")
    implementation("androidx.core:core-ktx:1.3.0-alpha01")
    implementation("androidx.constraintlayout:constraintlayout:1.1.3")
    implementation("com.squareup.okhttp3:okhttp:4.2.1")
    implementation("com.squareup.okhttp3:logging-interceptor:4.2.1")
    implementation("com.squareup.retrofit2:retrofit:2.6.2")
    implementation("com.squareup.picasso:picasso:2.71828")
    implementation("androidx.recyclerview:recyclerview:1.1.0")
    implementation("com.google.code.gson:gson:2.8.6")
    implementation("com.squareup.retrofit2:converter-gson:2.6.2")

    // Koin for Kotlin
    implementation("org.koin:koin-core:2.0.1")
    // Koin extended & experimental features
    implementation("org.koin:koin-core-ext:2.0.1")
    // Koin for Unit tests
    testImplementation("org.koin:koin-test:2.0.1")
    // Koin for Android
    implementation("org.koin:koin-android:2.0.1")
    // Koin AndroidX Scope features
    implementation("org.koin:koin-androidx-scope:2.0.1")
    // Koin AndroidX ViewModel features
    implementation("org.koin:koin-androidx-viewmodel:2.0.1")

    testImplementation("junit:junit:4.12")
    androidTestImplementation("androidx.test:runner:1.2.0")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.2.0")
}

tasks.withType<KotlinCompile>().all {
    kotlinOptions.freeCompilerArgs += "-Xuse-experimental=kotlin.Experimental"
}
